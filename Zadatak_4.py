import numpy as np
import matplotlib.pyplot as plt
broj=1000
x=30
i=0
sv_vector=np.zeros(broj)
dev_vector=np.zeros(broj)
while(broj!=0):    
    a=np.random.choice([1,2,3,4,5,6], size=(x,)) 
    sv=sum(a)/x         
    a_dev=np.std(a)     
    dev_vector[i]=a_dev
    sv_vector[i]=sv
    broj=broj-1
    i=i+1
plt.hist(dev_vector) 
plt.hist(sv_vector)  
plt.show()